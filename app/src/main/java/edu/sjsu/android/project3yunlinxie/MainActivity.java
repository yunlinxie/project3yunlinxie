package edu.sjsu.android.project3yunlinxie;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Build;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity  {
    // TODO: add a MyView attribute

    private MyView myview;

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: construct the MyView object
        myview = new MyView(this);

        // TODO: and set content view to MyView instead of the layout file.
        setContentView(myview);
    }
    @Override
    protected void onStart() {
        super.onStart();
        // TODO: use the startSimulation method in MyView to register the listener
        myview.startSimulation();


    }

    @Override
    protected void onStop() {
        super.onStop();
        // TODO:  use the stopSimulation method in MyView to unregister the listener
        myview.stopSimulation();
    }
}