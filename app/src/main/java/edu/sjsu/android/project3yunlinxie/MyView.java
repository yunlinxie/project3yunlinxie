package edu.sjsu.android.project3yunlinxie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.view.Display;
import android.view.View;

import androidx.annotation.RequiresApi;

import static android.graphics.Paint.Align.CENTER;
import static android.view.Surface.ROTATION_0;
import static android.view.Surface.ROTATION_90;

public class MyView extends View implements SensorEventListener {

    // You can change the ball size if you want
    private static final int BALL_SIZE = 100;
    private Bitmap field;
    private Bitmap ball;
    private float XOrigin;
    private float YOrigin;
    private float horizontalBound;
    private float verticalBound;
    private final Particle mBall = new Particle();
    private String text = "Yunlin Xie";


    // Paint object is used to draw your name
    private Paint paint = new Paint();


    // TODO: set attributes (objects) needed for sensor
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Display mDisplay;
    private float mSensorX;
    private float mSensorY;
    private float mSensorZ;
    private long mSensorTimestamp;
    // HINT: 2 of them are classes in the sensor framework
    //      1 is used for getting the rotation from "natural" orientation
    //      4 of them are used for the sensor data (3 axes + timestamp)

    @RequiresApi(api = Build.VERSION_CODES.R)
    public MyView(Context context) {
        super(context);
        mDisplay = context.getDisplay();
        // You will see errors here because there are no image files yet.
        // Add the images under drawable folder to get rid of the errors.
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        ball = Bitmap.createScaledBitmap(b, BALL_SIZE, BALL_SIZE, true);
        field = BitmapFactory.decodeResource(getResources(), R.drawable.field);
        // TODO: Initialize the objects related to the sensor except for sensor data
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    // TODO: set XOrigin, YOrigin, HorizontalBound and VerticalBound based on the screen size
    // HINT: there is a method in View class related to the size of the screen.
    //      The method's parameters include width and height of the screen.
    //      So override that method to set the 4 attributes.
    //      Also, you should createScaledBitmap for the basketball field,
    //      use the width and height of the screen in this method.


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        XOrigin = w * 0.5f;
        YOrigin = h * 0.4f - BALL_SIZE;
        horizontalBound = ((w - BALL_SIZE) * 0.5f);
        verticalBound = ((h - BALL_SIZE) * 0.5f);
        field = Bitmap.createScaledBitmap(field,w,h,true);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the field and the ball
        canvas.drawBitmap(field, 0, 0, null);
        canvas.drawBitmap(ball,
                (XOrigin - BALL_SIZE / 2) + mBall.mPosX,
                (YOrigin - BALL_SIZE / 2) - mBall.mPosY, null);
        // TODO: "draw" your name (make it easy to see)

        paint.setTypeface(Typeface.create(Typeface.DEFAULT_BOLD, Typeface.BOLD));
        paint.setStrokeWidth(12);
        paint.setTextSize(80);
        paint.setColor(Color.WHITE);

        canvas.drawText(text,350,180, paint);
        // TODO: control the ball based on the sensor data using methods in Particle class
        mBall. updatePosition(mSensorX, mSensorY, mSensorZ, mSensorTimestamp);
        mBall.resolveCollisionWithBounds(horizontalBound,verticalBound);
        invalidate();
    }

    public void startSimulation() {
        // TODO: Register sensor event listener
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSimulation() {
        // TODO: Unregister sensor event listener
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // TODO: get the sensor data (set them to the corresponding class attributes)
        // Remember to interpret the data as discussed in Lesson 12 page 16.
        if (mDisplay.getRotation() == ROTATION_0){
            mSensorX = sensorEvent.values[0];
            mSensorY = sensorEvent.values[1];
            mSensorZ = sensorEvent.values[2];
        }
        else if  (mDisplay.getRotation() == ROTATION_90) {
            mSensorX = -sensorEvent.values[1];
            mSensorY = sensorEvent.values[0];
            mSensorZ = sensorEvent.values[2];

        }
        mSensorTimestamp = sensorEvent.timestamp;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
